# Scaffolder

The **scaffolder**, otherwise known as **software templates** by the
[Backstage docs](https://backstage.io/docs/features/software-templates/software-templates-index),
is a way to create objects based on defined templates. The sample template that
ships with a standard Backstage installation is a Node.js app.

In the case of Digital Rebar, templates can be used to quickly spin up DRP
objects like Clusters. This tutorial will explain how to create such a template,
utilizing the Digital Rebar template actions provided by the [backend] plugin.

In this tutorial, we will be creating a Backstage template that walks a user
through the process of spinning up a cluster in DRP. It is assumed that the
steps in [backend] are followed so that the required
[custom actions](#custom-actions) are available.

## Adding templates

This tutorial operates under the assumption that you have an understanding of
how to create new templates and add them to your Backstage instance. This is
described in the [official Backstage documentation][bs-adding-templates]. A
description of the template schema can also be found
[here][bs-writing-templates].

Let's start by creating a directory to store our templates. From our Backstage
instance...

```sh
mkdir -p packages/backend/templates/drp
```

Now, let's create our template.

```sh
touch packages/backend/templates/drp/create-cluster.yaml
```

Next, we need to register the template with our Backstage instance. This is
described in the [documentation link][bs-adding-templates] above.

Open `app-config.yaml` in the editor of your choice, and look for the `catalog:`
object at the root of the configuration file.

By default, an example template is registered in a standard, unmodified
Backstage installation. It looks like this:

**Note: this is an example template for your reference! Do not add this to the
config file.**

```yml
catalog:
  # ...
  locations:
    # ...

    # Local example template
    - type: file
      target: ../../examples/template/template.yaml
      rules:
        - allow: [Template]
```

Let's add another one of those `- type: file` entries. It will reference the
template we created.

```yml
    - type: file
      target: templates/drp/create-cluster.yaml
      rules:
        - allow: [Template]
```

The template should now be registered in your Backstage instance, except our
template is blank. Let's fill it out with our intended functionality.

## Custom actions

The Backstage scaffolder/templates interact with the Digital Rebar API through
custom actions. In the examples linked above, some actions include
`fetch:template`, `fetch:plain`, `publish:github`, and `catalog:register`. These
are actions that will be taken as steps are reached. The [backend] plugin is
responsible for implementing these custom actions, which allow us to use things
like creating clusters in our case.

Please complete the [backend] tutorial if you have not already.

## Writing the template

Below is a blob of `yaml` source that we will set our template to. It will be
broken down and explained afterwards. Set the template at
`packages/backend/templates/drp/create-cluster.yaml` to the following source:

<!-- prettier-ignore -->
```yaml
apiVersion: scaffolder.backstage.io/v1beta3
kind: Template
metadata:
  name: drp-create-cluster
  title: Create DRP Cluster
  description: Create a Digital Rebar Cluster.
spec:
  owner: user:guest
  type: service

  parameters:
    - title: Cluster information
      required: ["name", "broker"]
      properties:
        name:
          title: Name
          type: string
          description: The name of the Cluster to add.
          ui:autofocus: true
        broker:
          title: Broker
          type: string
          description: The broker for the Cluster.
  steps:
    - id: create
      name: Create Cluster
      action: drp:clusters:create # our custom action!
      input:
        # the following fields are directly
        # passed to the created DRP object
        Name: ${{ parameters.name }}
        Params:
          broker/name: ${{ parameters.broker }}
  output:
    links:
      - title: Jump to Cluster in UX
        url: https://portal.rackn.io/#/e/${{ steps.create.output.endpoint }}/clusters/${{ steps.create.output.object.Uuid }}
```

Now, let's look at each section of our template individually.

<br />

```yaml
apiVersion: scaffolder.backstage.io/v1beta3
kind: Template
metadata:
  name: drp-create-cluster
  title: Create DRP Cluster
  description: Create a Digital Rebar Cluster.
spec:
  owner: user:guest
  type: service
```

The section above is simple template metadata. More information on this is
described in [Backstage documentation][bs-writing-templates].

<br />

```yaml
parameters:
  - title: Cluster information
    required: ['name', 'broker']
    properties:
      name:
        title: Name
        type: string
        description: The name of the Cluster to add.
        ui:autofocus: true
      broker:
        title: Broker
        type: string
        description: The broker for the Cluster.
```

The section above declares the parameters of the template. In our case, we need
parameters for the name of the Cluster, and for the broker the Cluster should be
assigned.

Again, [documentation][bs-writing-templates] will help with adding more fields
as necessary. You will likely want to include more parameters to construct more
interesting objects, such as cluster size.

<br />

```yaml
steps:
  - id: create
    name: Create Cluster
    action: drp:clusters:create # our custom action!
    input:
      # the following fields are directly
      # passed to the created DRP object
      Name: ${{ parameters.name }}
      Params:
        broker/name: ${{ parameters.broker }}
```

The `steps` object is responsible for declaring what the template actually
_does_ when it is constructed. Here, it calls the `drp:clusters:create` action
we set up in the [backend], and creates a preliminary DRP Cluster object with
the data from our parameters.

<br />

<!-- prettier-ignore -->
```yaml
  output:
    links:
      - title: Jump to Cluster in UX
        url: https://portal.rackn.io/#/e/${{ steps.create.output.endpoint }}/clusters/${{ steps.create.output.object.Uuid }}
```

Finally, the `output` object describes what is displayed to the user after the
template is finished being constructed. Here, we have it set to show a link to
the user, "Jump to Cluster in UX," that should link to the new object in the
portal.

The `${{ steps.create.output.endpoint }}` notation is templating syntax for the
output of the `create` step (`steps.create.output`). Our `endpoint` field on
this `steps.create.output` object is filled in by our [custom action in the
backend plugin][backend], as is the `steps.create.output.object` object, which
represents our new DRP Cluster as returned by the DRP API.

## Usage

At this point, we have created and registered our template with our Backstage
instance.

Before testing, ensure that you have set up
[authorization with DRP](backend.md#drp-authorization).

Spin up your Backstage instance...

```sh
$ yarn dev
```

Then click the `Create` button from the opened Catalog page. Your new template
should be visible. Fill it out and create the Cluster!

That said, continue on to the [frontend] if you [added router extensions](backend.md#adding-router-extensions) when setting up the backend!

[bs-adding-templates]:
  https://backstage.io/docs/features/software-templates/adding-templates
[bs-writing-templates]:
  https://backstage.io/docs/features/software-templates/writing-templates
[backend]: backend.md
[frontend]: frontend.md
