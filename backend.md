# Backend

The backend plugin is responsible for providing custom actions to the
[scaffolder] and API functionality to the [frontend].

In this tutorial, we will be setting up the aforementioned plugin to work with
our two other components.

We will:

<!-- prettier-ignore -->
1. [Create the plugin](#creating-the-plugin)
2. Optionally [add custom actions](#adding-custom-actions) for the [scaffolder]
3. Optionally [add router extensions](#adding-router-extensions) for the [frontend]
4. [Export our plugin changes](#exporting-our-plugin-functionality)
5. [Authorize our Backstage instance with DRP](#drp-authorization)
6. [Register the plugin in Backstage](#register-the-plugin)

**While you can choose to omit adding custom actions or router extensions, it is
recommended that you do both so that you get a full sense for Digital Rebar's
power with Backstage.**

**Note:** A sample, reference backend plugin is available at
<https://gitlab.com/zfranks/backstage-plugin-digitalrebar-backend>.

## Creating the plugin

The official documentation for creating a backend plugin is available
[here](https://backstage.io/docs/plugins/backend-plugin), but we will walk
through it for our cases.

Start from the root directory of your Backstage instance.

```sh
yarn new --select backend-plugin
```

Let's name (ID) our plugin `digitalrebar`. This will create a package at
`plugins/digitalrebar-backend`. The package will be named
`@internal/plugin-digitalrebar-backend`. You are free to rename these after this
tutorial.

## Using the Digital Rebar TypeScript API

From the root of your Backstage instance, issue the following command to add the
DRP TS API to your plugin.

```sh
yarn add --cwd plugins/digitalrebar-backend @rackn/digitalrebar-api
```

This will add the official
[Digital Rebar TypeScript API](https://www.npmjs.com/package/@rackn/digitalrebar-api)
to your backend plugin. It can be used as a lightweight wrapper for DRP REST API
calls, which will greatly simplify the implementation of our
[custom actions](#adding-custom-actions) and the functionality our [frontend]
should present to the user.

## Adding custom actions

First, from the root of your Backstage instance, issue the following command to add the
Backstage Scaffolder Utilities module to your plugin:

```sh
yarn add --cwd plugins/digitalrebar-backend @backstage/plugin-scaffolder-node
```

Next, open up your Backstage instance in the text editor of your choice. In
your backend plugin's `src` directory at `plugins/digitalrebar-backend/src`, create a new directory
`actions`:

```sh
mkdir plugins/digitalrebar-backend/src/actions
```

Create a new file `clusters.ts` in this folder. It will hold the functionality
for our `drp:clusters:create` action we will use in the [scaffolder].

```sh
touch plugins/digitalrebar-backend/src/actions/clusters.ts
```


Set `clusters.ts` to the following. We will break it down after the source.

```ts
import type { JsonValue } from '@backstage/types';
import { createTemplateAction } from '@backstage/plugin-scaffolder-node';
import DRApi, { DRMachine } from '@rackn/digitalrebar-api';
import { scaffolderActionsExtensionPoint } from '@backstage/plugin-scaffolder-node/alpha';
import {
  coreServices,
  createBackendModule,
} from '@backstage/backend-plugin-api';

export const scaffolderModule = createBackendModule({
  pluginId: 'scaffolder',
  moduleId: 'custom-extension',
  register(env) {
    env.registerInit({
      deps: {
        scaffolder: scaffolderActionsExtensionPoint,
        config: coreServices.rootConfig,
      },
      async init({ scaffolder, config }) {
        // DRP endpoints use self-signed certificates
        // you may want to set this manually, but it is here
        // for development purposes
        process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

        scaffolder.addActions(
          createTemplateAction({
            id: 'drp:clusters:create',
            schema: {
              input: {
                type: 'object',
              },
            },
            handler: async ctx => {
              const endpoint = config.getString('digitalrebar.endpoint');
              const token = config.getString('digitalrebar.token');
              const api = new DRApi(endpoint);
              api.setToken(token);

              ctx.logger.info('Creating cluster...');

              const response = await api.clusters.create(
                ctx.input as unknown as DRMachine,
              );

              ctx.logger.info(
                `Created cluster with UUID ${response.data.Uuid}`,
              );
              ctx.output('object', response.data as unknown as JsonValue);
              ctx.output('endpoint', endpoint);
            },
          }),
        );
      },
    });
  },
});

```

Ignoring our import section, which are required to use Backstage's APIs, let's look at the entire thing broken down:

<br />

```ts
export const scaffolderModule = createBackendModule({
  pluginId: 'scaffolder',
  moduleId: 'custom-extension',
  register(env) {
    env.registerInit({
      deps: {
        scaffolder: scaffolderActionsExtensionPoint,
        config: coreServices.rootConfig,
      },
      async init({ scaffolder, config }) {
```

This section sets up the a backend module to create action extensions to Backstage's scaffolder. The `deps` containing the `scaffolder` and `config` are pieces of other services running within Backstage that can be interacted with from the module. They are provided to the module in the `async init({ scaffolder, config })` function.

<br />

```ts
  // DRP endpoints use self-signed certificates
  // you may want to set this manually, but it is here
  // for development purposes
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
```

This section sets the `NODE_TLS_REJECT_UNAUTHORIZED` environment variable to
`0`, as DRP endpoints use self-signed certificates. You may want to set this
elsewhere, or remove it entirely if your DRP endpoint is properly signed.

<br />

```ts
  scaffolder.addActions(
    createTemplateAction({
      id: 'drp:clusters:create',
      schema: {
        input: {
          type: 'object',
        },
      },
```

We are adding a new action definition to the scaffolder service.
Actions are created using the Backstage-provided `createTemplateAction`. We will
call it `drp:clusters:create`. Its input schema is simply an object with no
known parameters; it simply gets shipped to our DRP API as the object we are
trying to create. In this way, the input object of this action can be an entire
DRP Cluster.

<br />

```ts
      handler: async ctx => {
        const endpoint = config.getString('digitalrebar.endpoint');
        const token = config.getString('digitalrebar.token');
        const api = new DRApi(endpoint);
        api.setToken(token);
```

In this section, we grab the `endpoint` and `token` config options from our
`app-config.yaml` (see the section on [DRP authorization](#drp-authorization))
and we give them to our `DRApi` so that we can make calls to our DRP endpoint.

<br />

<!-- prettier-ignore -->
```ts
        ctx.logger.info('Creating cluster...');

        const response = await api.clusters.create(
          ctx.input as unknown as DRMachine
        );
```

Here, we note to the user that we are creating the cluster. Then, we call
`api.clusters.create` with our input object.

Note the TypeScript that is happening here (`as unknown as DRMachine`).
Backstage provides `input` as a `JsonValue`, which cannot be properly cast to
`DRMachine`. We trust the conversion, so we assume `input` is `unknown`, and
then `DRMachine`, so that the value can be passed into the `create` method.

<br />

<!-- prettier-ignore -->
```ts
        ctx.logger.info(`Created cluster with UUID ${response.data.Uuid}`);
        ctx.output('object', response.data as unknown as JsonValue);
        ctx.output('endpoint', endpoint);
```

We notify the user that the cluster has been created, and its UUID is fetched
from `response.data.Uuid`. The entire Cluster object that was created is
available at `response.data`, for that matter.

We set our outputs:

- `object` is our `response.data` described above. Note again the TypeScript
  casting that is used for the inverse case.
- `endpoint` is our endpoint we got from our configuration a few sections above.
  Again, see the section on [DRP authorization](#drp-authorization).

## Adding router extensions

In order to provide functionality to the [frontend], we need to extend
Backstage's router in our backend plugin.

Create a new folder `service` in your backend plugin's `src` directory at `plugins/digitalrebar-backend/src` **if it
does not exist**:

```sh
mkdir -p plugins/digitalrebar-backend/src/service
```

Then, open `plugins/digitalrebar-backend/src/service/router.ts` and set it or edit it to the
following. Again, we will break it down afterwards.

```ts
import { errorHandler } from '@backstage/backend-common';
import { LoggerService } from '@backstage/backend-plugin-api';
import { Config } from '@backstage/config'; // add this line
import DRApi, { DRWorkOrder } from '@rackn/digitalrebar-api'; // add this line
import express from 'express';
import Router from 'express-promise-router';

export interface RouterOptions {
  logger: LoggerService;
  config: Config; // add this line
}

export async function createRouter(
  options: RouterOptions,
): Promise<express.Router> {
  const { logger } = options;

  const router = Router();
  router.use(express.json());

  router.get('/health', (_, response) => {
    logger.info('PONG!');
    response.json({ status: 'ok' });
  });

  router.use(errorHandler());

  // --- add these lines ---

  // read token and endpoint from config
  const endpoint = options.config.getString('digitalrebar.endpoint');
  const token = options.config.getString('digitalrebar.token');

  // create a new DR Api with those credentials
  const api = new DRApi(endpoint);
  api.setToken(token);

  router.get('/clusters', async (_, response) => {
    response.json((await api.clusters.list({ aggregate: 'true' })).data);
  });

  router.delete('/clusters/:uuid', async (request, response) => {
    response.json((await api.clusters.delete(request.params.uuid)).data);
  });

  router.post('/work_orders', async (request, response) => {
    response.json((await api.workOrders.create(request.body)).data);
  });

  router.patch('/profiles/:name', async (request, response) => {
    response.json(
      (await api.profiles.patch(request.params.name, request.body)).data
    );
  });

  router.patch('/clusters/:uuid/scale', async (request, response) => {
    const { current, count } = request.body;

    const { data: cluster } = await api.clusters.get(request.params.uuid);
    const { data: profile } = await api.profiles.get(cluster.Name);

    if (!cluster.WorkOrderMode) {
      await api.clusters.patch(cluster.Uuid, [
        {
          op: 'test',
          path: '/WorkOrderMode',
          value: false as unknown as object,
        },
        {
          op: 'replace',
          path: '/WorkOrderMode',
          value: true as unknown as object,
        },
      ]);
    }

    await api.profiles.patch(
      cluster.Name,
      'cluster/count' in (profile.Params ?? {})
        ? [
            { op: 'test', path: '/Params/cluster~1count', value: current },
            { op: 'replace', path: '/Params/cluster~1count', value: count },
          ]
        : [{ op: 'add', path: '/Params/cluster~1count', value: count }]
    );

    const wo = await api.workOrders.create({
      Blueprint: 'universal-application-base-cluster',
      Machine: cluster.Uuid,
      Context: cluster.Context,
    } as DRWorkOrder);

    response.send(wo.data);
  });
  // --- add those lines

  return router;
}
```

Much of this is Backstage boilerplate, and is somewhat explained in the
[official documentation](https://backstage.io/docs/plugins/backend-plugin#developing-your-backend-plugin).
The parts we care about are below:

<br />

<!-- prettier-ignore -->
```ts
  const endpoint = options.config.getString('digitalrebar.endpoint');
  const token = options.config.getString('digitalrebar.token');

  const api = new DRApi(endpoint);
  api.setToken(token);
```

This should look familiar if you completed the
[custom actions](#adding-custom-actions) part of this tutorial.

<br />

<!-- prettier-ignore -->
```ts
  router.get('/clusters', async (_, response) => {
    response.json((await api.clusters.list({ aggregate: 'true' })).data);
  });
```

This call adds a listener to `GET /clusters` to our router. It returns a list of
Clusters from our DRP endpoint, thanks to the `api.clusters.list` method
provided by the [DRP TypeScript API](#using-the-digital-rebar-typescript-api).
We also set `aggregate: 'true'` here so that DRP responds with _all_ Params,
even those across inherited Profiles, as all Clusters have an associated
Profile.

<br />

<!-- prettier-ignore -->
```ts
  router.delete('/clusters/:uuid', async (request, response) => {
    response.json((await api.clusters.delete(request.params.uuid)).data);
  });
```

Like the call above, this will add a listener to `DELETE /clusters/:uuid`. It
will be responsible for deleting clusters given their UUID.

<br />

<!-- prettier-ignore -->
```ts
  router.post("/work_orders", async (request, response) => {
    response.json((await api.workOrders.create(request.body)).data);
  });
```

This adds a listener to `POST /work_orders`, which will allow us to send
arbitrary requests to create DRP Work Order objects to our DRP instance. We will
use it to create the work order that scales our cluster.

<br />

<!-- prettier-ignore -->
```ts
  router.patch("/profiles/:name", async (request, response) => {
    response.json(
      (await api.profiles.patch(request.params.name, request.body)).data
    );
  });
```

Likewise, this adds a listener to `PATCH /profiles/:name` that allows us to
patch existing profiles by their name. Again, this is used to set the cluster
size of our cluster as that information is not stored on the Cluster object
itself, but rather its associated Profile.

<br />

<!-- prettier-ignore -->
```ts
  router.patch('/clusters/:uuid/scale', async (request, response) => {
    const { current, count } = request.body;

    const { data: cluster } = await api.clusters.get(request.params.uuid);
    const { data: profile } = await api.profiles.get(cluster.Name);

    if (!cluster.WorkOrderMode) {
      await api.clusters.patch(cluster.Uuid, [
        { op: 'test', path: '/WorkOrderMode', value: false as unknown as object },
        { op: 'replace', path: '/WorkOrderMode', value: true as unknown as object },
      ]);
    }

    await api.profiles.patch(cluster.Name, 'cluster/count' in (profile.Params ?? {}) ? [
      { op: 'test', path: '/Params/cluster~1count', value: current },
      { op: 'replace', path: '/Params/cluster~1count', value: count },
    ] : [
      { op: 'add', path: '/Params/cluster~1count', value: count },
    ]);

    const wo = await api.workOrders.create({
      Blueprint: 'universal-application-base-cluster',
      Machine: cluster.Uuid,
      Context: cluster.Context,
    } as DRWorkOrder);

    response.send(wo.data);
  });
```

Finally, this last handler handles `PATCH /clusters/:uuid/scale` which scales
our cluster for us. It ensures WorkOrderMode is set on the cluster, which allows
us to run blueprints on it on the fly independent of a routine workflow. It
patches the Cluster Profile, changing its size. Then, it creates a WorkOrder
that runs `universal-application-base-cluster`, which is the Blueprint in DRP
that is used to re-provision and scale clusters, creating new machines or
removing old ones.

## Add config to router extension

Provide config to the router service, we have to add it as a dependency of the backend plugin. Add these lines to `plugins/digitalrebar-backend/src/plugin.ts` to ensure the router has access to the config.

```ts
import {
  coreServices,
  createBackendPlugin,
} from '@backstage/backend-plugin-api';
import { createRouter } from './service/router';

/**
 * digitalrebarPlugin backend plugin
 *
 * @public
 */
export const digitalrebarPlugin = createBackendPlugin({
  pluginId: 'drp',  // change this to drp
  register(env) {
    env.registerInit({
      deps: {
        httpRouter: coreServices.httpRouter,
        logger: coreServices.logger,
        config: coreServices.rootConfig, // add this line
      },
      async init({
        httpRouter,
        logger,
        config, // add this line
      }) {
        httpRouter.use(
          await createRouter({
            logger,
            config, // add this line
          }),
        );
        httpRouter.addAuthPolicy({
          path: '/health',
          allow: 'unauthenticated',
        });
      },
    });
  },
});

```

Our plugin is now prepared to listen to API calls from our [frontend].

## Exporting our plugin functionality

To make our backend functionality visible to the rest of Backstage, set your
plugin's `plugins/digitalrebar-backend/src/index.ts` to the following:

```ts
export * from './service/router';
export { digitalrebarPlugin as default } from './plugin';

// add this line to export actions
export * from './actions/clusters';
```

## DRP Authorization

In order for your Backstage instance to be able to make requests to DRP, you
need to add some information to your instance's `app-config.yaml`.

First, you'll need to get your endpoint. Your endpoint should be the IP address
followed by the port DRP listens to (by default, `8092`). It is the same as the
IP and port you use to log into the DRP UX. You'll replace `YOUR-ENDPOINT` in
the following `yaml` excerpt with this address.

Then, you'll need a DRP user's token that the plugin will make requests through.
You can find this by running the following command somewhere with `drpcli`
access (be sure to change `USERNAME` to the DRP username, default being
`rocketskates`):

`drpcli users token USERNAME ttl 9999999 | jq .Token -r`

Open `app-config.yaml` in the text editor of your choice, and add the following
to the bottom of the file (ensure there are no spaces before `digitalrebar:`):

```yaml
digitalrebar:
  endpoint: YOUR-ENDPOINT # example: 127.0.0.1:8092
  token: YOUR-TOKEN # from the command above
```

You will also need to add a file in your backend plugin. Create the file
`plugins/digitalrebar-backend/config.d.ts`

```sh
touch plugins/digitalrebar-backend/config.d.ts
```

And paste the following into it:

```ts
export interface Config {
  digitalrebar: {
    /**
     * The endpoint (IP and port) of the DRP instance.
     * @visibility frontend
     */
    endpoint: string;
    /**
     * The auth token of the DRP instance.
     * @visibility secret
     */
    token: string;
  };
}
```

This simply instructs Backstage to check for this config schema when it reads
its `app-config.yaml`. Finally, you need to register this schema file with your
plugin package. Open `plugins/digitalrebar-backend/package.json` and remove the
entry for `files`. Then, add the following to the root object:

<!-- prettier-ignore -->
```json
  "files": [
    "dist",
    "config.d.ts"
  ],
  "configSchema": "config.d.ts"
```

## Register the plugin

To register the plugin with Backstage, you'll have to modify one file.

Open `packages/backend/src/index.ts`, and add the following lines before `backend.start()` (near the end of the file)

```ts
// digitalrebar backend plugin
backend.add(import('@internal/backstage-plugin-digitalrebar-backend'));
// digitalrebar scaffolder actions
backend.add(
  import('@internal/backstage-plugin-digitalrebar-backend').then(m => ({
    default: m.scaffolderModule,
  })),
);
```

This will register the plugin router and custom actions with Backstage's API router.

## Verification

Now that we have finished creating a backend plugin, we can build it with `yarn dev`

Once it is running, you can check if your backend plugin is working by:

- Opening `localhost:7007/api/drp/health` and seeing `{"status":"ok"}`
- Opening `localhost:7007/api/drp/clusters` and seeing a "Missing credentials" error

    > **Note**: The Backstage API automatically adds authentication, so this is an **expected** Backstage error rather than a Digital Rebar Error. We will use this API with authentication in the next steps.

## Usage

As an independent component, the backend plugin does not do much. That said,
continue on to the [scaffolder] (if you
[added custom actions](#adding-custom-actions)) or to the [frontend] (if you
[added router extensions](#adding-router-extensions)). If you did both, choose
either!

[scaffolder]: scaffolder.md
[frontend]: frontend.md
