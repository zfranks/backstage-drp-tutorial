# Frontend

The frontend plugin is used to display information in the Backstage portal, and
uses the [backend] plugin to communicate with Digital Rebar.

In this tutorial, we will create and set up the frontend plugin so that we can
view a list of Clusters in our DRP endpoint, as well as a way to delete
clusters.

If you have not yet completed the [backend] tutorial, please complete that
first, as there is functionality required by the frontend!

This tutorial assumes a basic understanding of [React](https://reactjs.org/).

**Note:** A sample, reference frontend plugin is available at
<https://gitlab.com/zfranks/backstage-plugin-digitalrebar>.

## Creating the plugin

The official documentation for creating a frontend plugin is available
[here](https://backstage.io/docs/plugins/backend-plugin), but we will walk
through it for our cases.

Start from the root directory of your Backstage instance.

```sh
yarn new --select plugin
```

Use the same ID as our [backend] plugin, `digitalrebar`.

This will create a package at `plugins/digitalrebar`. The package will be named
`@internal/plugin-digitalrebar`. You are free to rename these after this
tutorial.

## Using the Digital Rebar TypeScript API

From the root of your Backstage instance, issue the following command to add the
DRP TS API to your plugin.

```sh
yarn add --cwd plugins/digitalrebar @rackn/digitalrebar-api
```

This will add the official
[Digital Rebar TypeScript API](https://www.npmjs.com/package/@rackn/digitalrebar-api)
to your frontend plugin. It can be used to provide typings for objects within the backend
api such as `DRMachine`.

## Adding the plugin page

Create the `Clusters` directory inside the plugin's `components` directory to house
the components we're going to be adding:

```sh
mkdir plugins/digitalrebar/src/components/Clusters
```

### Table Components

Create `plugins/digitalrebar/src/components/Clusters/Table.tsx`

```sh
touch plugins/digitalrebar/src/components/Clusters/Table.tsx
```

And populate it with the following source:

```tsx
import { Progress, Table } from '@backstage/core-components';
import {
  configApiRef,
  errorApiRef,
  fetchApiRef,
  useApi,
} from '@backstage/core-plugin-api';
import { IconButton, Link } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import RemoveIcon from '@material-ui/icons/Remove';
import Alert from '@material-ui/lab/Alert';
import { DRMachine } from '@rackn/digitalrebar-api';
import React, { useState } from 'react';
import useAsync from 'react-use/lib/useAsync';

const ClusterActions: React.FC<{ data: DRMachine; type: 'row' | 'group' }> = ({
  data,
}) => {
  const config = useApi(configApiRef);
  const [loading, setLoading] = useState(false);
  const [deleted, setDeleted] = useState(false);
  const { fetch } = useApi(fetchApiRef);

  const onDelete = () => {
    if (loading) return;
    setLoading(true);

    fetch(
      `${config.getString('backend.baseUrl')}/api/drp/clusters/${data.Uuid}`,
      { method: 'DELETE' },
    )
      .then(() => {
        setDeleted(true);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  if (deleted) return <i>Deleted.</i>;
  if (loading) return <Progress />;

  return (
    <IconButton size="small" onClick={onDelete}>
      <DeleteIcon />
    </IconButton>
  );
};

const ClusterCount: React.FC<{ data: DRMachine; type: 'row' | 'group' }> = ({
  data,
}) => {
  const config = useApi(configApiRef);
  const error = useApi(errorApiRef);
  const baseUrl = config.getString('backend.baseUrl');
  const { fetch } = useApi(fetchApiRef);

  const [loading, setLoading] = useState(false);
  const [count, setCount] = useState(
    (data.Params?.['cluster/count'] as number | undefined) ?? 0,
  );

  const onScale = (target: number) => {
    if (loading) return;
    if (target < 0) return;

    setLoading(true);
    fetch(`${baseUrl}/api/drp/clusters/${data.Uuid}/scale`, {
      method: 'PATCH',
      body: JSON.stringify({ current: count, count: target }),
      headers: { 'Content-Type': 'application/json' },
    })
      .then(async res => {
        if (res.ok) {
          setCount(target);
        } else {
          error.post({
            name: 'Cluster scale error',
            message: `Failed to scale the cluster ${data.Name} (${data.Uuid}).`,
          });
        }
      })
      .catch(() =>
        error.post({
          name: 'Cluster scale error',
          message: `Failed to scale the cluster ${data.Name} (${data.Uuid}).`,
        }),
      )
      .finally(() => setLoading(false));
  };

  return (
    <>
      <IconButton size="small" onClick={() => onScale(count - 1)}>
        <RemoveIcon />
      </IconButton>
      <span>{count}</span>
      <IconButton size="small" onClick={() => onScale(count + 1)}>
        <AddIcon />
      </IconButton>
    </>
  );
};

export const ClustersTable = () => {
  const config = useApi(configApiRef);
  const endpoint = config.getString('digitalrebar.endpoint');
  const { fetch } = useApi(fetchApiRef);

  const { value, loading, error } = useAsync(async (): Promise<DRMachine[]> => {
    return await fetch(
      `${config.getString('backend.baseUrl')}/api/drp/clusters`,
    ).then(r => r.json());
  }, []);

  if (loading) {
    return <Progress />;
  } else if (error) {
    return <Alert severity="error">{error.message}</Alert>;
  }

  return (
    <Table
      title="Clusters"
      options={{ search: false, paging: false }}
      columns={[
        {
          title: 'Name',
          render: (data, _) => (
            <Link
              href={`https://portal.rackn.io/#/e/${endpoint}/clusters/${data.Uuid}`}
            >
              {data.Name}
            </Link>
          ),
        },
        {
          title: 'Size',
          render: (data, type) => <ClusterCount data={data} type={type} />,
        },
        { title: 'Broker', field: 'Params.broker/name' },
        {
          title: 'Actions',
          render: (data, type) => <ClusterActions data={data} type={type} />,
        },
      ]}
      data={value ?? []}
    />
  );
};
```

Let's break down each of the components individually.

#### `ClusterActions`

This component provides the delete button, allowing us to delete Clusters from
the table. Of its source, most notably is the call to `/api/drp/clusters/:uuid`,
which we added in our [backend] plugin.

#### `ClusterCount`

This component shows the size of the cluster, but also includes a plus and minus
button around the count that will automatically scale the cluster when they are
clicked. It does this through the call to `PATCH /api/drp/clusters/:uuid/scale`,
which we also added in our [backend] plugin.

#### `ClustersTable`

A table of Clusters. Includes some basic information about each cluster, as well
as the `ClusterActions` component allowing us to remove clusters by their row.

`useApi` and `useAsync` are hooks provided by Backstage, as well as the `Table`
component (and its child `TableColumn`s). There is an official
[Backstage tutorial](https://backstage.io/docs/tutorials/quickstart-app-plugin)
that goes into further detail on these pieces.

The `const { fetch } = useApi(fetchApiRef)` call gives the [FetchApi](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API) authorized access to Backstage's backend APIs via the Backstage Frontend. This is how we can access `/api/drp/clusters` without getting a `Missing Credentials` error.

You'll notice the call to `useAsync` uses the `GET /clusters` route we added in
our [backend] plugin. The preceding `/drp` was declared when we
[registered the backend plugin with Backstage](backend.md#if-you-made-router-extensions).

### View Components

Create `plugins/digitalrebar/src/components/Clusters/View.tsx`

```sh
touch plugins/digitalrebar/src/components/Clusters/View.tsx
```

And populate it with the following source:

```tsx
import {
  Content,
  ContentHeader,
  Header,
  HeaderLabel,
  Page,
  SupportButton,
} from '@backstage/core-components';
import { Grid } from '@material-ui/core';
import React from 'react';
import { ClustersTable } from './Table';

export const ClustersView = () => (
  <Page themeId="tool">
    <Header title="Welcome to Digital Rebar!">
      <HeaderLabel label="Owner" value="RackN" />
      <HeaderLabel label="Lifecycle" value="Alpha" />
    </Header>
    <Content>
      <ContentHeader title="Digital Rebar">
        <SupportButton>Some description goes here.</SupportButton>
      </ContentHeader>
      <Grid container spacing={3} direction="column">
        <Grid item>
          <ClustersTable />
        </Grid>
      </Grid>
    </Content>
  </Page>
);
```

This is just React filler that follows Backstage's conventions for plugin pages.
It should be reminiscent of the included boilerplate `ExampleComponent`.

## Registering the plugin with Backstage

Conveniently, [creating the plugin](#creating-the-plugin) automatically
registers it within Backstage's frontend router (mostly). However, you will need
to change the exported component to be the one you created rather than the
included example component.

Open `plugins/digitalrebar/src/plugin.ts`, and change these lines:

```ts
component: () =>
  import('./components/ExampleComponent').then(m => m.ExampleComponent),
```

to this:

```ts
component: () =>
  import('./components/Clusters/View').then(m => m.ClustersView),
```

## DRP Authorization

Similarly to what you did for the [backend] plugin, you will need to create a
`config.d.ts` file for this plugin.

You can copy the via through the CLI:

```bash
cp plugins/digitalrebar-backend/config.d.ts plugins/digitalrebar/config.d.ts
```

Or you can create the file `plugins/digitalrebar/config.d.ts`, and paste the following into
it:

```ts
export interface Config {
  digitalrebar: {
    /**
     * The endpoint (IP and port) of the DRP instance.
     * @visibility frontend
     */
    endpoint: string;
    /**
     * The auth token of the DRP instance.
     * @visibility secret
     */
    token: string;
  };
}
```

This simply instructs Backstage to check for this config schema when it reads
its `app-config.yaml`. Finally, you need to register this schema file with your
plugin package. Open `plugins/digitalrebar/package.json` and remove the entry
for `files`. Then, add the following to the root object:

<!-- prettier-ignore -->
```json
  "files": [
    "dist",
    "config.d.ts"
  ],
  "configSchema": "config.d.ts"
```

### Register it in another instance

If you would like to register it with another Backstage instance after
publishing the plugin, check the diffs in `packages/app/src/App.tsx` after
creating the plugin. It should be the addition of an `import` line and a new
`Route` entry.

## Usage

Start your Backstage instance with

```sh
yarn dev
```

and navigate to `http://localhost:3000/digitalrebar` to see your changes.

Optionally, create a Cluster with the template you set up in the [scaffolder]
tutorial, and report back to see if it is now in the frontend table. Check to
see that the delete icon deletes your Cluster.

[backend]: backend.md
[scaffolder]: scaffolder.md
